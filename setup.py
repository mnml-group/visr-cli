from setuptools import setup

setup(
    name='visrcli',
    version='0.0.1',
    packages=['visr'],
    install_requires=[
        'aiohttp',
        'aiohttp[speedups]'
    ],
    python_requires=">=3.9",
    entry_points={
        'console_scripts': [
            'visr-cli = visr.main:main',
        ]
    }
)
