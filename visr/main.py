import asyncio
import logging
import signal
import os
import json
from . import api
from . import runner
from . import config
import argparse


def load_api_config(args: argparse.Namespace) -> config.ApiConfig:
    conf_dict = {}
    if os.path.isfile(args.credentials_file):
        with open(args.credentials_file, 'r') as fp:
            conf_dict = json.load(fp)
            # TODO validate conf dict
    conf_dict['client_secret'] = args.client_secret or conf_dict['client_secret']
    conf_dict['client_id'] = args.client_id or conf_dict['client_id']
    conf_dict['host'] = args.host or conf_dict.get('host') or 'https://api.visr.app'
    return config.ApiConfig(conf_dict)


def load_runner_config(args: argparse.Namespace) -> config.RunnerConfig:
    conf_dict = {}
    if os.path.isfile(args.runner_file):
        with open(args.runner_file, 'r') as fp:
            conf_dict = json.load(fp)
            # TODO validate conf dict
    conf_dict['team_id'] = args.team_id or conf_dict['team_id']
    conf_dict['runner_id'] = args.runner_id or conf_dict['runner_id']
    return config.RunnerConfig(conf_dict)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--client-secret', dest="client_secret", type=str, metavar="<client-secret>")
    parser.add_argument('--client-id', dest="client_id", type=str, metavar="<client-id>")
    parser.add_argument(
        '--credentials-file', dest="credentials_file",
        type=str, default='creds.json', metavar='<file-path>'
    )
    parser.add_argument('--host', type=str, dest="host", metavar="<host-url>")
    subparser = parser.add_subparsers(dest='command', title="commands", metavar="")
    runner_parser = subparser.add_parser('runner', help="The runner daemon.")
    runner_parser.add_argument("runner_command", choices=["run"], default="run")
    runner_parser.add_argument(
        "--runner-file", dest="runner_file", metavar="<file-path>", type=str, default="runner.json"
    )
    runner_parser.add_argument("--team-id", dest="team_id", metavar="<team-id>", type=str)
    runner_parser.add_argument("--runner-id", dest="runner_id", metavar="<runner-id>", type=str)
    args = parser.parse_args()
    api_conf = load_api_config(args)
    if args.command == 'runner':
        logging.basicConfig(level=logging.DEBUG)
        runner_conf = load_runner_config(args)
        visr_api = api.Api(api_conf)
        _runner = runner.Runner(runner_conf, visr_api)
        signal.signal(signal.SIGINT, lambda signame, frame: _runner.halt())
        signal.signal(signal.SIGTERM, lambda signame, frame: _runner.halt())

        print("Event loop running, press Ctrl+C to interrupt.")
        asyncio.run(_runner.run())


# if __name__ == '__main__':
    # api_config = load_api_config()
    # print(api_config.__dict__)
