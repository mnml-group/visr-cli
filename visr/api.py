from . import config as api_config
import aiohttp
import logging
import json
import pprint


# class JSONEncoder(json.JSONEncoder):
#
#     def default(self, obj):
#
#         # Match all the types you want to handle in your converter
#         if isinstance(obj, Result):
#             return obj.__dict__
#         # Call the default method for other types
#         return json.JSONEncoder.default(self, obj)


class Result:
    health_check_id: str
    start: str
    end: str
    error: str = None

    def __init__(self, health_check_id: str, start: str, end: str, error: str = None):
        self.health_check_id = health_check_id
        self.start = start
        self.end = end
        self.error = error


class Response:
    status: int
    json: dict
    headers: dict
    url: str

    def __init__(self, status: int, json: dict, headers: dict, url: str) -> None:
        self.status = status
        self.json = json
        self.headers = headers
        self.url = url

    @staticmethod
    def from_client_response(response: aiohttp.ClientResponse, json: dict):
        status = response.status
        headers = {row[0]: row[1] for row in response.headers.items()}
        url = str(response.url)
        return Response(status, json, headers, url)


class ApiException(Exception):
    pass


class Api:
    _config: api_config.ApiConfig
    _token: str = None

    def __init__(self, config: api_config.ApiConfig) -> None:
        self._config = config

    async def post(self, session: aiohttp.ClientSession, endpoint: str, json: dict, timeout: int = 60 * 5) \
            -> Response:
        """
        :raises asyncio.TimeoutError: on client side timeout.
        """
        timeout = aiohttp.ClientTimeout(total=timeout)
        url = self._config.host + endpoint
        headers = {'Authorization': f"Bearer {self._token}"} if self._token else {}
        try:
            logging.info(f"POST {url} {json}")
            async with session.post(
                    url,
                    json=json,
                    timeout=timeout,
                    headers=headers
            ) as response:
                return Response.from_client_response(response, await response.json())
        except Exception as e:
            pprint(e)
            raise e
            # raise ApiException(f"Exception {type(e).__name__}")

    async def refresh_token(self, session: aiohttp.ClientSession) -> None:
        body = {
            "client_id": self._config.client_id,
            "client_secret": self._config.client_secret,
            "grant_type": "client_credentials"
        }
        try:
            response = await self.post(session, "/token", body)
        except Exception as e:
            logging.error(f"Failed to refresh access token. Error: {type(e).__name__}")
            raise ApiException(f"Failed to refresh access token. Error: {type(e).__name__}")
        if response.status != 201:
            logging.error(f"Failed to refresh access token. Status: {response.status}")
            raise ApiException(f"Failed to refresh access token. Status: {response.status}")
        self._token = response.json.get('access_token')

    async def sync_runner(self, session, team_id: str, runner_id: str, results: list):
        json_results = [r.__dict__ for r in results]
        return await self.post(session, f"/teams/{team_id}/runners/{runner_id}/actions/sync", {"results": json_results})

    @staticmethod
    def session() -> aiohttp.ClientSession:
        return aiohttp.ClientSession()
