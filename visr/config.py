# config = {
#     'sync_every':  1,
#     'host': "https://api.visr.app",
#     "team_id": "publisher",
#     "runner_id": "ByI7YLkm",
#     "client_id": "79e53c6f95fb6d64",
#     "client_secret": "630e12bc78aa43414bceae6622b261785a976643712586bdd65f69e5fd0ddb8b"
# }


class Config(object):
    def __init__(self, config: dict):
        self._config = config  # set it to conf

    def get(self, name):
        if name not in self._config.keys():  # we don't want KeyError
            return None   # just return None if not found
        return self._config[name]


class ApiConfig(Config):
    @property
    def host(self) -> str:
        return self.get('host')

    @property
    def client_id(self) -> str:
        return self.get('client_id')

    @property
    def client_secret(self) -> str:
        return self.get('client_secret')

    def merge(self, config: dict):
        """
        wtf python, cant type hint current class
        https://www.python.org/dev/peps/pep-0484/#the-problem-of-forward-declarations
        """
        self._config |= config


class RunnerConfig(Config):
    @property
    def team_id(self) -> str:
        return self.get('team_id')

    @property
    def runner_id(self) -> str:
        return self.get('runner_id')

    @property
    def sync_every(self) -> str:
        return self.get('sync_every')
