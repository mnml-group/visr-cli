import aiohttp
import asyncio
from datetime import datetime, timezone
import logging

from . import config
from . import api


class SyncException(Exception):
    pass


class Runner:
    _config: config.RunnerConfig
    _api: api.Api
    _schedule: list = []
    _results: list = []
    _sync_failures: int = 0
    _last_minute: int = -1
    _halt: bool = False

    def __init__(self, runner_config: config.RunnerConfig, visr_api: api.Api):
        self._config = runner_config
        self._api = visr_api

    async def sync(self, session: aiohttp.ClientSession) -> None:
        try:
            response = await self._api.sync_runner(
                session, self._config.team_id, self._config.runner_id, self._results
            )
            if response.status == 401:
                await self._api.refresh_token(session)
                response = await self._api.sync_runner(
                    session, self._config.team_id,
                    self._config.runner_id, self._results)
            if response.status == 200:
                logging.info(f"Received schedule:")
                logging.info(response.json['schedule'])
                self._schedule = response.json['schedule']
                self._results = []
            else:
                logging.error(f"Sync failure response {response.status}")
                self._sync_failures += 1
        except Exception as e:
            # TODO catch more granular exception types
            logging.error(f"Sync Exception {type(e).__name__}")
            self._sync_failures += 1
            raise e

    @staticmethod
    async def check(session: aiohttp.ClientSession, item: dict) -> api.Result:
        timeout = aiohttp.ClientTimeout(total=item.get('timeout'))
        error = None
        start = datetime.now(tz=timezone.utc).isoformat(timespec='milliseconds')
        try:
            async with session.get(item.get('endpoint'), timeout=timeout) as resp:
                await resp.text()
                if resp.status >= 400:
                    error = 'http_error'
                logging.info(f"{item.get('endpoint')} responded with {resp.status}")
        except TimeoutError:
            error = 'timeout'
            logging.info(f"{item.get('endpoint')} timed out.")
        except aiohttp.ClientConnectorError:
            logging.info(f"{item.get('endpoint')} was unable to connect.")
            error = 'unresolvable_host'
        end = datetime.now(tz=timezone.utc).isoformat(timespec='milliseconds')
        return api.Result(item.get('id'), start, end, error)

    async def run(self):
        async with aiohttp.ClientSession() as session:
            await self._api.refresh_token(session)  # may want to write a token to disk.

        while not self._halt:
            now = datetime.now()
            current_minute = (60 * now.hour + now.minute)
            if current_minute != self._last_minute:
                async with self._api.session() as session:
                    self._results.extend(await asyncio.gather(
                        *[self.check(session, item) for item in self._schedule
                          if current_minute % item['frequency'] == 0]
                    ))
                    await self.sync(session)
                    self._last_minute = current_minute

    def halt(self) -> None:
        print("Halting loop, can take up to 15 seconds.")
        self._halt = True

